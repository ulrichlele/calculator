package com.lele.test;

import com.lele.calculator.ExpressionParser;
import com.lele.calculator.SimpleExpressionParser;
import com.lele.calculator.throwables.DivisionByZeroException;
import com.lele.calculator.throwables.ExpressionSyntaxException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Unit test of SimpleExpressionParserTest implementing  ExpressionParser
 *
 * @author Ulrich LELE
 */
public class SimpleExpressionParserTest {

    static ExpressionParser parser;

    @BeforeAll
    static   void runsBefore(){
        parser = new SimpleExpressionParser();
    }

    @Test
    void onePlusOne() {
        assertEquals(parser.parse("1+1"), 2d);
    }

    @Test
    void onePlusOneV2() {
        assertEquals(parser.parse("1+1.1 - 0.1"), 2.0d);
    }

    @Test
    void onePlusTwo() {
        assertEquals(parser.parse("1 + 2"), 3d);
    }

    @Test
    void onePlusMinusOne() {
        assertEquals(parser.parse("1 + -1"), 0d);
    }

    @Test
    void fiveMinusFour() {
        assertEquals(parser.parse("5-4"), 1d);
    }

    @Test
    void fiveTimeTwo() {
        assertEquals(parser.parse("5*2"), 10d);
    }

    @Test
    void twoPlusFiveAllTimeThree() {
        assertEquals(parser.parse("(2+5)*3"), 21d);
    }

    @Test
    void tenDevidedByTwo() {
        assertEquals(parser.parse("10/2"), 5d);
    }

    @Test
    void fiveTimeTwoPlusTwoPlusFive() {
        assertEquals(parser.parse("2+2*5+5"), 17d);
    }

    @Test
    void twoPointEightTimeTreeMinusOne() {
        assertEquals(parser.parse("2.8*3-1"), 7.399999999999999d);
    }

    @Test
    void twoExponentialEight() {
        assertEquals(parser.parse("2^8"), 256d);
    }

    @Test
    void twoExponentialEightTimeFiveMinusOne() {
        assertEquals(parser.parse("2^8*5-1"), 1279d);
    }
    @Test
    void fiveTimesTwoExponentialThree() {
        assertEquals(parser.parse("5*2^3"), 40d);
    }

    @Test
    void fiveTimesTwoExponentialThreeTimesSquareRootOfNine() {
        assertEquals(parser.parse("5*2^3*sqrt(9)"), 120d);
    }

    @Test
    void fiveTimesTwoExponentialThreeDividedByTheValueOfTreePlusOneThenTimesSquareRootOfFour() {
        assertEquals(parser.parse("5*2^3/(3+1)*sqrt(4)"), 20d);
    }

    @Test
    void squareRootOfFour() {
        assertEquals(parser.parse("sqrt(4)"), 2d);
    }

    @Test
    void oneDividedByZero() {
        assertThrows(DivisionByZeroException.class, () -> parser.parse("1/0"));
    }



    @Test
    void expressionSyntaxException() {
        assertThrows(ExpressionSyntaxException.class, () -> parser.parse(" 34 + !"));
    }

    @Test
    void eightyNineTimesTen(){
        assertEquals(parser.parse("89 *10"), 890d);
    }


}
