package com.lele.calculator;

import java.util.Scanner;

public class Calculator {

    private static final String EXIT_CHAR = "x";
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String welcomeMessage = "Welcome to the calculator. Type "+EXIT_CHAR+" to exit";
        System.out.println(welcomeMessage);
        String inputMessage= "---\nEnter an expression (x to exit): ";
        System.out.print(inputMessage);
        ExpressionParser parser = new SimpleExpressionParser();

        while(true){
            String input = scanner.nextLine();
            if(isExitChar(input)){
                System.out.println("Good bye");
                break;
            }
            System.out.println("Result : " + parser.parse(input));
            System.out.print(inputMessage);
        }




        // closes the scanner
        scanner.close();
    }

    private static boolean isExitChar(String input){
        return input == null || input.trim().equalsIgnoreCase(EXIT_CHAR);
    }
}
