package com.lele.calculator.util;

/**
 * Common string utility methods
 * @author Ulrich LELE
 */
public class StringUtils {

    public static boolean isEmpty(String txt){
        return txt == null || txt.trim().isEmpty();
    }
}
