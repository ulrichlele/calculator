package com.lele.calculator;

/**
 * Enumeration of operators
 * @author Ulrich LELE
 */
public enum Operator {
    Addition('+'), Subtraction('-'), Division('/'), Multiplication('*');

    char charactar;

    Operator(char charactar){
        this.charactar = charactar;
    }

    public char getCharactar(){
        return charactar;
    }

}
