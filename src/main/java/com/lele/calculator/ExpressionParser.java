package com.lele.calculator;

/**
 * Arithmetic expression parser.
 * @author Ulrich LELE
 * @version 1
 */
public interface ExpressionParser {
    /**
     * Parse a string arithmetic expression to a double. <br>
     * Permit spaces between tokens <br>
     * Respect the priorities; Bracket, Multiplication, Division, Addition, Subtraction. <br>
     * Takes into account functions  & exponential. <br>
     *
     * @param expression to be parsed.
     * @return returns double value of the parsed expression.
     */
    double parse(String expression);
}
