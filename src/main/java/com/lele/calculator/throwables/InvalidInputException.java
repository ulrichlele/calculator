package com.lele.calculator.throwables;

/**
 * Invalid input Exception
 * @author Ulrich LELE
 */
public class InvalidInputException extends RuntimeException{

    public InvalidInputException(String message) {
        super(message);
    }
}
