package com.lele.calculator.throwables;

/**
 * Expression syntax exception
 * @author Ulrich LELE
 */
public class ExpressionSyntaxException extends RuntimeException{
    public ExpressionSyntaxException(String message) {
        super(message);
    }
}
