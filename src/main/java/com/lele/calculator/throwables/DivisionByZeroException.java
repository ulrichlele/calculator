package com.lele.calculator.throwables;

/**
 * Division by zro exception
 * @author  Ulrich LELE
 */
public class DivisionByZeroException extends RuntimeException{
    public DivisionByZeroException(String message) {
        super(message);
    }
}
