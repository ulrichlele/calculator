package com.lele.calculator;

import com.lele.calculator.throwables.DivisionByZeroException;
import com.lele.calculator.throwables.ExpressionSyntaxException;
import com.lele.calculator.throwables.InvalidInputException;
import com.lele.calculator.util.StringUtils;

/**
 * Arithmetic expression parser.
 * Parse a string arithmetic expression to a number.
 * @author Ulrich LELE
 */
public class SimpleExpressionParser implements ExpressionParser{
    // Definition of functions
    public static final String FUNCTION_SQRT = "sqrt";

    public static final String FUNCTION_ABS = "abs";

    // list of operators ordered by priorities

    private int cursorPos = -1;
    private int currentChar;
    private String expression;

     
    public SimpleExpressionParser(){

    }

    /**
     * Move the cursor to the next character & get the character at the position of the current cursor.
     */
    void moveToNextChar() {
        ++cursorPos;
        currentChar = cursorPos< expression.length() ? expression.charAt(cursorPos) : -1;
    }


    /**
     * Parse a string arithmetic expression to a double. <br>
     * Permit spaces between tokens <br>
     * Respect the priorities; Bracket, Multiplication, Division, Addition, Subtraction. <br>
     * Takes into account functions  & exponential. <br>
     *
     * @param expression String expression to be parsed.
     * @return returns double value of the parsed expression.
     */
    @Override
    public double parse(String expression) {
        this.expression = preprocess(expression);
        this.cursorPos = -1;
        this.currentChar = -1;
        return parseExpression();
    }

    /**
     * Checks if the expression is not empty, trim and converts expression to lowercase
     * @param expression
     * @return expression
     * @throws InvalidInputException
     */
    private String preprocess(String expression) throws InvalidInputException {
        if(StringUtils.isEmpty(expression))
            throw  new InvalidInputException("Empty expression string");
        return expression.trim().toLowerCase();
    }

    /**
     * Check if the next character(space excluded) is equal to the passed token character.
     * @param charToCheck character to check.
     * @return true if the character passed as parameter is equal to the next character (space excluded).
     */
    boolean processable(int charToCheck) {
        while (currentChar== ' ')
            moveToNextChar();
        if (currentChar== charToCheck) {
            moveToNextChar();
            return true;
        }
        return false;
    }

    /**
     *
     * @return returns the result of the evaluation.
     */
    double parseExpression() {
        cursorPos = cursorPos != -1 ? --cursorPos : cursorPos;
        moveToNextChar();
        double x = parseFactor();
        for (int index = cursorPos; index < expression.length(); index++) {
            for(Operator operator : Operator.values()) {
                Boolean parsableByOperator = processable(operator.getCharactar());
                if (parsableByOperator) {
                    x = switch (operator) {
                        case Multiplication -> x *= parseFactor();
                        case Division -> {
                            double parsedResult = parseFactor();
                            if (parsedResult == 0)
                                throw new DivisionByZeroException("Division by zero at position " + cursorPos);
                            x /= parsedResult;
                            yield x;
                        }
                        case Addition ->{
                            if(higherOperatorsExist()){
                                x += parseExpression();
                            }
                            else
                                x += parseFactor();
                            yield x;
                        }
                        case Subtraction ->{
                            if(higherOperatorsExist()){
                                x -= parseExpression();
                            }else
                                x -= parseFactor();
                            yield x;
                        }
                        default -> x;
                    };

                }
            }
        }

        return x;
    }


    private boolean higherOperatorsExist(){
        return expression.indexOf('*', cursorPos) >0 || expression.indexOf('/', cursorPos) > 0;
    }



    /**
     * Identify the factor and stream the input to identify the current token.
     * @return
     */
    double parseFactor() {
        //check for unary expression
        if (processable('+'))
            return +parseFactor();
        if (processable('-'))
            return -parseFactor();

        double x;
        int startPos = this.cursorPos;
        //check for parenthesis
        if (processable('(')) { // parentheses
            x = parseExpression();
            if (!processable(')'))
                throw new ExpressionSyntaxException("Missing ')' from position "+startPos);
        } // if in range of numbers
        else if ((currentChar>= '0' && currentChar<= '9') || currentChar== '.') {
            while ((currentChar>= '0' && currentChar<= '9') || currentChar== '.')
                moveToNextChar();
            x = Double.parseDouble(expression.substring(startPos, this.cursorPos));
        } else if (currentChar>= 'a' && currentChar<= 'z') { // functions
            while (currentChar>= 'a' && currentChar<= 'z')
                moveToNextChar();
            String functionName = expression.substring(startPos, this.cursorPos);
            if (processable('(')) {
                x = parseExpression();
                if (!processable(')'))
                    throw new ExpressionSyntaxException("Missing ')' after the argument of" + functionName + " at position "+ cursorPos);
            } else {
                x = parseFactor();
            }
            x = switch(functionName){
                case FUNCTION_SQRT -> Math.sqrt(x);
                case FUNCTION_ABS -> Math.abs(x);
                default -> throw new RuntimeException("Unknown function name: '" + functionName +  "( at position "+ startPos);
            }; 
        } else {
            char invalidChar = (char) currentChar;
            throw new ExpressionSyntaxException("Unexpected character '" +Character.valueOf(invalidChar).toString() +  "' at position "+ cursorPos);
        }
        if (processable('^'))
            x = Math.pow(x, parseFactor()); // exponentiation
        return x;
    }
}
