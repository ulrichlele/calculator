# Simple Calculator
     * Parse a string arithmetic expression to a double.
     * Permit spaces between tokens.
     * Respect the priorities; Bracket, Multiplication, Division, Addition, Subtraction.
     * Takes into account functions  & exponential.


## Run

	Run the app from the Calculor main method.

## Unit testing

	Unit test case are available for some sample expressions.
 